package com.yalla.selenium.api.base;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class AnnotationModule extends SeleniumBase{
	@DataProvider(name="fetchData")
	@Parameters({"url","username","password"})
  @BeforeMethod  
	  public void beforeMethod(String url,String uname12,String pwd) 
	  {
		  startApp("Chrome",url);
		
			//* Login page User name and Password
		  
		  WebElement uname = locateElement("id","username");
		  clearAndType(uname, uname12);
		  
		  WebElement password = locateElement("id","password");
		  clearAndType(password, pwd);
		  
		  
		  WebElement submitbutton = locateElement("class","decorativeSubmit");
		  click(submitbutton);
		  
		  WebElement CRMlink = locateElement("link","CRM/SFA");
		  click(CRMlink);
		  
  }
  @AfterMethod
  public void afterMethod() {
	  close();
  }

  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

  @BeforeTest
  public void beforeTest() {
  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

}
