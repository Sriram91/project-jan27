package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.UnexpectedTagNameException;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import report.ReportTest;

public class SeleniumBase extends ReportTest implements Browser, Element{

	public RemoteWebDriver driver;
	WebDriverWait wait;
	
	@Override
	public void click(WebElement ele) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			System.out.println("The Element "+ele+" clicked");
			reportStep("Element " +ele+ "is clicked ","Pass");
		} catch (StaleElementReferenceException e) {
			System.err.println("The Element "+ele+" could not be clicked");
			reportStep("Element " +ele+ "is not clicked ","fail");
			throw new RuntimeException();
			// Why do we need this runtime exception here? i have not written this exception in most of the methods in this file. 
		/*if you want to stop the execution after enter into catch block 
			use throw new RuntimeException();*/
		}
		takeSnap();// when do we need to take snapshot? 
		/*for each and every step log if its pass or failed also*/
	}

	@Override
	public void append(WebElement ele, String data) {
		String text =""; 
		try {
			text= ele.getText();
			ele.sendKeys(text+" "+data);
			reportStep("element text: "+text+" appended successfuly", "pass");
		} catch (NoSuchElementException e) {  
			reportStep("element text: "+text+" not appended successfuly", "fail");
		}  

	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			ele.clear();
			reportStep("The clear is done Successfully", "pass");
		}catch (InvalidElementStateException e){
			reportStep("The Element "+ele+" is not Interactable", "fail");
			throw new RuntimeException();
		}finally {//do we need finally here? why?
			/*you need some code to be execute even thaw if its pass or fail means u have to use 
			  finally block. here if step pass i need snap and step fails also i need snap*/
			takeSnap();
		}
			
		}
	

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data :"+data+" entered Successfully", "pass");
		} catch (ElementNotInteractableException e) {
			reportStep("The Element "+ele+" is not Interactable", "fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}

	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		String text="";
		try {
			 text = ele.getText();
			 reportStep("text get from "+ele+ "is successful","pass");
		}catch(Exception e) {
			System.err.println("");
			reportStep("text get from "+ele+ "is not successful","fail");
		}
		
		return text;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		try {
			String cssValue = ele.getCssValue("color");
			reportStep("back ground colour is received ", "pass");
			return cssValue;
		} catch (Exception e) {
			reportStep("back ground colour is not received ", "fail");
			e.printStackTrace();
			//when i do try catch, e.printAtackTrace() is getting displayed;Why? what it does here?
			/*it will give the exception log in console when u get an exception
			 Instead of printStackTrace() u can use System.err.println() u can give ur customized msg*/
			return cssValue;
		}
	}

	@Override
	public String getTypedText(WebElement ele) {
		try {
			String attribute = ele.getAttribute("value");
			reportStep("Text received from WebElement "+ele+ " is Successfuly", "pass");
			return attribute;
		} catch (Exception e) {
			reportStep("Text received from WebElement "+ele+ "is not Successfuly", "fail");
			e.printStackTrace();
			return attribute;
		}
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select se = new Select(ele);
			se.selectByVisibleText(value);
			reportStep("Drop down selection using text" +value+ "is done successfully", "Pass");
		} catch (NoSuchElementException e) {
			reportStep("Drop down selection using text" +value+ "is not done successfully", "fail");
		} catch (UnexpectedTagNameException e) {
			reportStep("Drop down selection using text" +value+ "is not done successfully", "fail");
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		try {
			Select se =new Select(ele);
			se.selectByIndex(index);
			reportStep("Dropdown selection using Index is successful", "pass");
		} catch (NoSuchElementException e) {
			reportStep("Dropdown selection using Index is not successful", "fail");			
			e.printStackTrace();
		}

	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		try {
			Select se=new Select(ele);
			se.selectByValue(value);
			reportStep("Dropdown selection using value is successful","pass");
		} catch (NoSuchElementException e) {
			reportStep("Dropdown selection using value is not successful","fail");
			e.printStackTrace();
		}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		try {
			if (ele.getText().equals(expectedText)) {
				System.out.println("text matched");
				return true;
			} else {
				System.out.println("text not matched");
				return false;
			}
		} catch (Exception e) {
			reportStep("verification of exact text not done successfully","fail");
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String text=ele.getText();
		try {
			if(text.contains(expectedText)) {
				return true;			
			}else
			return false;
		} catch (Exception e) {
			reportStep("Text "+ele +"does not contain "+expectedText+"" ,"fail");
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			if (ele.getAttribute(attribute).equals(value)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			reportStep("given attributes value "+value+" is not exacty as "+attribute+ "","fail");
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		try {
			if(ele.getAttribute(attribute).contains(value)) {
				reportStep("given attributes value "+value+" is partially exactly as "+attribute+ "","pass");
			}else
				reportStep("given attributes value "+value+" is not partially exactly as "+attribute+ "","fail");
		} catch (Exception e) {
			
			reportStep("given attributes value "+value+" is not partially exactly as "+attribute+ "","fail");
			e.printStackTrace();
		}

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		try {
			boolean displayed = ele.isDisplayed();
			if (displayed == true) {
				
				reportStep(ele +"is displayed in the Dom", "pass");
				return true;
			} else {
				reportStep(ele +"is not displayed in the Dom", "fail");
				return false;
			}
		} catch (Exception e) {
			reportStep(ele +"is not displayed in the Dom", "fail");
			return false;
		}
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		try {
			if(!ele.isDisplayed()) {
				reportStep(ele + "is diappeared", "pass");
				return true;
			}
			
			else {
				reportStep(ele + "is not diappeared", "fail");
				return false;
			}
		} catch (Exception e) {
			reportStep(ele + "is not diappeared", "fail");
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		try {
			if(ele.isEnabled()) {
				reportStep("input element "+ele +"filed is enabed","pass");
				return true;
			}else {
				reportStep("input element "+ele +"filed is not enabed","fail");
				return false;
			}
			
		} catch (Exception e) {
			reportStep("input element "+ele +"filed is not enabed","fail");
			
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			if(ele.isSelected()) {
				reportStep("input element "+ele +"filed is selcted","pass");
				return true;			
			}else {
				reportStep("input element "+ele +"filed is not selected","fail");
				return false;
			}
		} catch (Exception e) {
			reportStep("input element "+ele +"filed is not selectable","fail");
			e.printStackTrace();
			return false;
		}
		}
		
	

	@Override
	public void startApp(String url) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		try {
			driver=new ChromeDriver();
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("Web page opened Successfully","pass");
		} catch (Exception e) {
			reportStep("url is not valid","false");
			e.printStackTrace();
		}
		

	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				//driver = new ChromeDriver();
				ChromeOptions op = new ChromeOptions();
				op.addArguments("--disable-notifications");
				driver = new ChromeDriver(op);
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("webpage opened successfully","pass");
		} catch (Exception e) {
			System.err.println("The Browser Could not be Launched. Hence Failed");
			reportStep("webpage is not opened successfully","fail");
			throw new RuntimeException();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			
		}
			reportStep("Element located successfully","pass");
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
			reportStep("Element is not located","fail");
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		WebElement findElementById;
		try {
			findElementById = driver.findElementById(value);
			reportStep("element located successfully", "pass");
			return findElementById;
		} catch (NoSuchElementException e) {
			reportStep("Not able to locate element for "+value,"fail");
			e.printStackTrace();
			return findElementById;
		}
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch(type.toLowerCase()) {
			case "id": return driver.findElementsById(value);
			case "name": return driver.findElementsByName(value);
			case "class": return driver.findElementsByClassName(value);
			case "link": return driver.findElementsByLinkText(value);
			case "xpath": return driver.findElementsByXPath(value);
			}
			reportStep("Element location identified sucessfully","pass");
		} catch (NoSuchElementException e) {
			System.err.println("The Element with locator:"+type+" Not Found with value: "+value);
			reportStep("Element location not identified sucessfully","fail");
			throw new RuntimeException();
		}
		return null;
	}

	@Override
	public void switchToAlert() {
	try {
		 wait.until(ExpectedConditions.alertIsPresent());
		 driver.switchTo().alert();
	} catch (Exception e) {
		reportStep("Aert not found","Pass");
		e.printStackTrace();
	}

	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		try {
			driver.switchTo().alert().accept();
			reportStep("Alert accepted successfully","Pass");
		} catch (NoAlertPresentException e) {
			reportStep("Alert not accepted","fail");
			//what if the alert does not pop up some times? can we write alert not available here?
			e.printStackTrace();
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			reportStep("Alert dismissed Successfully","pass");
		} catch (Exception e) {
			reportStep("Alert is not dismissed","fail");
			e.printStackTrace();
		}
		

	}

	@Override
	public String getAlertText() {
		String text="";
		try {
			text = driver.switchTo().alert().getText();
			reportStep("Alert text received successfully","pass");
			return text;
		} catch (Exception e) {
			reportStep("No text available in the alert","fail");
			e.printStackTrace();
			return text;
		}
	}

	@Override
	public void typeAlert(String data) {
		try {
			driver.switchTo().alert().sendKeys(data);
			reportStep("Alert text passed successfully","pass");
		} catch (Exception e) {
			reportStep("Alert text not passed","fail");
		}

	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		try {
			Set<String> allWindows = driver.getWindowHandles();
			List<String> lst = new ArrayList<>();
			lst.addAll(allWindows);
			driver.switchTo().window(lst.get(index));
			reportStep("Switched successfully to the window at the index "+index + " ","pass");
		} catch (Exception e) {
			reportStep("Not Switched to the window at the index "+index + " ","fail");
			e.printStackTrace();
		}

	}

	@Override
	public void switchToWindow(String title) {
		try {
			String title2 = driver.getTitle();
			driver.switchTo().window(title2);
			reportStep("Switch to window done successfully" ,"pass");
		} catch (Exception e) {
			reportStep("Switch to window is not done successfully" ,"fail");
			e.printStackTrace();
		}
		
	
	}

	@Override
	public void switchToFrame(int index) {
		try {
			driver.switchTo().frame(index);
			reportStep("Switched to frame at "+index+ "","pass");
		} catch (Exception e) {
			reportStep("Switched to frame at "+index+ "is not done","fail");
			e.printStackTrace();
		}

	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			reportStep("Switched to frame of "+ele+ "","pass");
		} catch (Exception e) {
			reportStep("Switched to frame of "+ele+ "is not done","fail");
			e.printStackTrace();
		}
	}

	@Override
	public void switchToFrame(String idOrName) {
		try {
			driver.switchTo().frame(idOrName);
			reportStep("Switched to frame of "+idOrName+ "","pass");
		} catch (Exception e) {
			reportStep("Switched to frame of "+idOrName+ "is not done","fail");
			e.printStackTrace();
		}

	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			reportStep("Frame switched to first frame ","pass");
		} catch (Exception e) {
			reportStep("Frame is not switched to first frame ","fail");
			e.printStackTrace();
		}

	}

	@Override
	public boolean verifyUrl(String url) {
		String currentUrl = driver.getCurrentUrl();
		try {
			if (currentUrl.equals(url)) {
				reportStep("current url is equal to expected url ","pass");
				return true;
			} else {
				reportStep("current url is not equal to expected url ","fail");
				return false;
			}
		} catch (Exception e) {
			reportStep("current url is not equal to expected url ","fail");
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		String title2 = driver.getTitle();
		try {
			if(title2.equals(title)) {
				reportStep("current title is equal to expected title ","pass");
				return true;
			}
				else {
					reportStep("current title is not equal to expected title ","fail");
					return false;
				}
		} catch (Exception e) {
			reportStep("current title is not equal to expected title ","fail");
			e.printStackTrace();
		}
				return false;
	}
	
	
	
	int i =0;
	@Override
	
	public void takeSnap() {
		// TODO Auto-generated method stub
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dec = new File("./snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, dec);
			reportStep("Snapshot of the browser captured Successfully","pass");
		} catch (IOException e) {
			reportStep("Snapshot of the browser is not captured ", "false");
			e.printStackTrace();
		}
		i++;

	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		try {
			driver.close();
			reportStep("Active browser is closed ","pass");
		} catch (Exception e) {
			reportStep("Active browser is not closed ","fail");
			e.printStackTrace();
		}

	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		try {
			driver.quit();
			reportStep("all the browser are closed ","pass");
		} catch (Exception e) {
			reportStep("all the browser are not closed ","fail");
			e.printStackTrace();
		}
	}
}


