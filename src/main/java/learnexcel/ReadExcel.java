package learnexcel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static Object[][] ReadExcel() throws IOException {
		// TODO Auto-generated method stub
		
		XSSFWorkbook wbook=new XSSFWorkbook("./Data/createlead.xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();
		System.out.println("Row Count is:"+rowcount);
		int colcount = sheet.getRow(0).getLastCellNum();
		System.out.println("Col count is"+colcount);
		Object[][] data=new Object[rowcount][colcount];
		for (int i = 1; i <=rowcount ; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < colcount; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				System.out.println(stringCellValue);
			}
		}
		return data;
		

	}

}
