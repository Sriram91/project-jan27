package testcases;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotation;
import com.yalla.selenium.api.base.SeleniumBase;


	public class MergeLead extends Annotation {
		
	@BeforeTest(groups="smoke")
		public void setData() {
			testcaseName="MergeLead";
			testcaseDec="Merge lead into one lead";
			author= "sriram";
			category="smoke";
			excelfilename = "MergeLead";
		}
	@Test(groups="smoke", dataProvider="fetchData")
	public void Mergelead(String FName,String FRS) throws InterruptedException
	{
					
					//6. Click Leads link
		WebElement Leadslink = locateElement("link","Leads");
		click(Leadslink);
					//7. Click Merge leads
		WebElement Mergelead = locateElement("link","Merge Leads");
		click(Mergelead);
					//8. Click on Icon near From Lead
		WebElement icon = locateElement("xpath","//img[@alt='Lookup']");
		click(icon);
		
					//9. Move to new window
					switchToWindow(1);
					//10.Enter FirstName
					
			WebElement firstname = locateElement("xpath","//input[@name='firstName']");
			clearAndType(firstname, FName);
					
					//11. Click Find Leads button
					WebElement findleads = locateElement("xpath","//button[text()='Find Leads']");
					click(findleads);
					Thread.sleep(3000);
					
					//12. Click First Resulting lead
					WebElement fristresults = locateElement("link",FRS);
					click(fristresults);
				
				//13. Switch back to primary window
					switchToWindow(0);
					
					
					// 14. Click on Icon near To Lead
					WebElement iconnear = locateElement("xpath","(//img[@alt='Lookup'])[2]");
					click(iconnear);
		
					//15. Move to new window
					switchToWindow(1);
					
					//16. Enter Lead ID
					WebElement leadID = locateElement("xpath","//input[@name='firstName']");
					clearAndType(leadID, FName);
					
					//17. Click Find Leads button
					WebElement findslead = locateElement("xpath","//button[text()='Find Leads']\")");
					click(findslead);
	
					//18. Click First Resulting lead
					
					WebElement firstresults = locateElement("link",FRS);
					click(fristresults);
			
					//19. Switch back to primary window
					switchToWindow(0);
					
					//20. Click Merge
					WebElement Meg = locateElement("link","Merge");
					click(Meg);
					
					//21. Accept Alert
					acceptAlert();
				
					//22. Click Find Lead
					WebElement findLead1 = locateElement("link","Find Leads");
					click(findLead1);
					
					//23. Enter From Lead ID
					WebElement fromlead = locateElement("xpath","//input[@name='id']");
					clearAndType(fromlead, "5000");
					
					//24. Click Find Leads
					WebElement find33 = locateElement("xpath","//button[text()='Find Leads']");
					click(find33);
					
					//25.Verify error msg
					WebElement errordata = locateElement("xpath","//div[text()='No records to display']");
					getElementText(errordata);
					System.out.println(errordata);
					
					//26. Close the browser (Do not log out)
					driver.close();			
		}

	}	