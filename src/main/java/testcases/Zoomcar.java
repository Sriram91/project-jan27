package testcases;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.AnnotationCar;

public class Zoomcar extends AnnotationCar{
	
	@BeforeClass
	public void SetData()
	{
		testcaseName="ZoomCar";
		testcaseDec="Testing a ZoomCar application";
		author= "sriram";
		category="smoke";
	}
	
	@Test
 public void Zoomcar1()
	 {
	
		WebElement Startlink = locateElement("link","Start your wonderful journey");
		click(Startlink);
		
		WebElement Popularpickpoint = locateElement("xpath","//div[@class='items']");
		click(Popularpickpoint);
		
		WebElement NextButton = locateElement("xpath","//button[text()='Next']");
		click(NextButton);
		
		//Specify date as tommorrow
		
		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);
		int tommorrow=Integer.parseInt(today)+1;
		System.out.println(tommorrow);
		
		//Click on the date 
		
WebElement datefunc = locateElement("xpath","//div[@class=\"day picked low-price\"]");
click(datefunc);

//confirm Next button 

WebElement CalNext = locateElement("xpath","//button[text()='Next']");
click(CalNext);

// click on the date

WebElement confrmdate = locateElement("xpath","//div[@class='day picked low-price']");
click(confrmdate);


//click the done status

WebElement donestatus = locateElement("xpath","//button[text()='Done']");
click(donestatus);

//capture the number of results

WebElement captureresults = locateElement("xpath","//div[@class='price']");
getElementText(captureresults);
System.out.println(captureresults);

WebElement hightolowprice  = locateElement("xpath","//div[@class='item active-input']");
click(hightolowprice);

WebElement price = locateElement("xpath","//div[@class='price']");
getElementText(price);
System.out.println(price);




	}

}
