package report;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BasicReport  {
	static ExtentHtmlReporter reporter;
	static ExtentReports extent;
	public ExtentTest test;
	public String testcaseName, testcaseDec, author, category;
	public static String excelfilename;

	@BeforeSuite(groups= "any")
	public void startReport() {
		reporter = new ExtentHtmlReporter("./reports/result.html");
		reporter.setAppendExisting(true); 
		extent   = new ExtentReports();
		extent.attachReporter(reporter);
	}

   @BeforeClass(groups= "any")
	public void report() throws IOException {
		test = extent.createTest(testcaseName, testcaseDec);
	    test.assignAuthor(author);
	    test.assignCategory(category);  
	}
    
    
    @AfterSuite(groups= "any")
    public void stopReport() {
    	extent.flush();
    }
}


